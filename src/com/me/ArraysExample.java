package com.me;

public class ArraysExample {

    public static void main (String[] args) {

        String name = new String("some name");
        String[] names = new String[5];
        int[] ages = new int[3];

        String[] places = {"Manila", "Warsaw", "Berlin", "Paris", "Madrid", "Tokyo", "New York"};

        for (int place = 0; place < places.length -1; place++) {
            System.out.println("We're in: " + places[place] + " and we go to:" + places[place+1]);
        }

        for (String place : places) {

        }

        ages[0] = 1;
        ages[1] = 18;
        ages[2] = 65;

//        [0 1 2 3 4 5]

        names[0] = "John";
        names[1] = "Bob";
        names[2] = "Anna";
        names[3] = "Rob";
        names[4] = "Patrick";

        System.out.println("Third name is:" + names[2]);

        if(names.length > 3){
            System.out.println("we have " + names.length + " names ");

        }

        for (int index = 0; index < names.length; index++) {
            System.out.println("Current name: " + names[index]);
        }

        String response = "";
        while (!response.equalsIgnoreCase("finish")) {
        }
    }
}
