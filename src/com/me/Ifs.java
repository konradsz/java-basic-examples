package com.me;

public class Ifs {

    public static void main(String[] args) {
        System.out.println("Start of main");
        boolean is_true = true;
        // boolean means true or false
        if (is_true) {
            System.out.println("It's true");
        }

        boolean is_false = false;
        if (is_false) {
            System.out.println("It's false");
        }

        if (24 > 50) {
            System.out.println("Impossible that 24 > 50");
        } else {
            System.out.println("I knew that 24 won't be > than 50");
        }

        int age = 24;
        if (age > 100) {
            System.out.println("Centurian");
        } else
            // After else comes a block of code i.e. code inside square brackets {}
//        { System.out.println("Not 100"); }

            // Or after else can come another if check.
            // So you end up chaining if's together
            if (age > 80) {
                System.out.println("Is 80");
            } else
//        {}
                if (age > 20) {
                    System.out.println("Is 20");
                }

        // && means AND
        if (true && true) {
            System.out.println("AND true + true");
        }
        if (true && false) {
            System.out.println("AND true + false");
        }

        // || means OR
        if (true || false) {
            System.out.println("OR true + false");
        }
        if (false || true) {
            System.out.println("OR false + true");
        }

        // honey
        boolean has_loyalty_card = false;
        boolean is_female = true;
        boolean is_adult = true;
        // kos
        boolean has_loyalty_card_kos = true;

        if (is_female && is_adult && has_loyalty_card) {
            System.out.println("Should get discount");
        } else {
            System.out.println("Won't get any discount");
        }


        if (is_adult && is_female && has_loyalty_card || has_loyalty_card_kos) {
            System.out.println("Using kos loyalty card to get discount");
        } else {
            System.out.println("No discount even with kos cards");
        }
    }
}