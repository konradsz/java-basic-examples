package com.me;

import java.util.Scanner;

public class ScannerExample {

    public static void  main (String [] args) {

        String username;

        System.out.println("Enter your name:");
        Scanner scanner = new Scanner(System.in);
        username = scanner.nextLine();

        System.out.println("You entered name: " + username);

        System.out.println("Are you a scholar? yes/no");
        String scholar = scanner.nextLine();

        if (scholar.toLowerCase().equals("yes")) {
            System.out.println("Your units: 1-50");
        } else {
            System.out.println("Are you regular or not? yes/no");
            String regular = scanner.nextLine();

            if (regular.toLowerCase().equals("yes")) {
                System.out.println("Your units: 1-25");
            } else {
                System.out.println("Your units: 1-10");
            }
        }
    }
}
