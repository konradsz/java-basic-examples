package com.me;


public class ScholarExample {

    public static void main (String [] args) {

        studentScore("John", true, true);
        studentScore("Patrick", false, true);
        studentScore("Mick", false, false);
    }

    static void studentScore (String name, boolean scholar, boolean regular) {
        System.out.println("Hi, " + name + " you have" + scoreChooser(scholar, regular) + " units");
    }

    static String scoreChooser (boolean scholar, boolean regular) {
        if (scholar) {
            return "1-50";
        } else if (regular) {
            return "1-25";
        } else {
            return  "1-10";
        }
    }
}
